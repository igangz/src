import RPi.GPIO as GPIO
import spidev
import time

RelayPin = 17
delay = 0.5
ldr_channel = 0

spi = spidev.SpiDev()
spi.open(0, 0)

SETTINGS = {
    "LIGHT_GPIO":       17,                     # GPIO Number (BCM) for the Relay
    "LIGHT_CHANNEL":    0,                      # of MCP3008
    "LIGHT_THRESHOLD":  100,                    # if the analog Threshold is below any of those, the light will turn on
}

def setup():
    GPIO.setwarnings(False)
    #set the gpio modes to BCM numbering
    GPIO.setmode(GPIO.BCM)
    #set RelayPin's mode to output,and initial level to LOW(0V)
    GPIO.setup(RelayPin,GPIO.OUT,initial=GPIO.LOW)

def print_message():
    print ('Program is running...')
    print ('Please press Ctrl+C to end the program...')
    print ('\n')

def readadc(adcnum):
    # read SPI data from the MCP3008, 8 channels in total
    if adcnum > 7 or adcnum < 0:
        return -1
    spi.max_speed_hz=1350000
    r = spi.xfer2([1,( 8 + adcnum) << 4, 0])
    data = ((r[1] & 3) << 8) + r[2]
    return data


def main():
    #print info
    print_message()
    while True:
        ldr_value = readadc(ldr_channel)
        print ("---------------------------------------")
        print("LDR Value: %d" % ldr_value)
        time.sleep(delay)
        
        if ldr_value <= SETTINGS["LIGHT_THRESHOLD"]:
            # turn light on
            GPIO.setup(SETTINGS["LIGHT_GPIO"], GPIO.OUT, initial=GPIO.LOW)
        else:
            # turn light off
            GPIO.setup(SETTINGS["LIGHT_GPIO"], GPIO.OUT, initial=GPIO.HIGH)
        
if __name__ == '__main__':
    try:
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)

        # execute functions
        main()
        
    except:
        GPIO.cleanup()